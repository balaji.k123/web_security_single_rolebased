package com.example.demo.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Entity.User;
import com.example.demo.Repository.UserRepository;
import com.example.demo.Service.AdminService;
import com.example.demo.Service.UserService;

@RestController
@RequestMapping("/")
public class UserController {

	@Autowired
	private BCryptPasswordEncoder PasswordEncoder;

	@Autowired
	public UserRepository userRepository;
	@Autowired
	private UserService userService;
	@Autowired
	private AdminService adminService;

	@PostMapping({ "/createNewUser" })
	public String user(@RequestBody User user) {
		String passWord = user.getPassWord();
		String PassWordEncrypt = PasswordEncoder.encode(passWord);
		user.setPassWord(PassWordEncrypt);
		userRepository.save(user);
		return "useraddededsuccessfully";
	}

	@GetMapping("/GetMessage")
	public String user1() {
		return "Welcome to admin";
	}
	@GetMapping("/userdeto")
	public List<User> user() {
		return this.adminService.getmsg();
	}
}
